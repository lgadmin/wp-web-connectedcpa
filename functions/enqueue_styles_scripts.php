<?php

function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], wp_get_theme()->get('Version') );
	
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i', false );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), false );
	wp_enqueue_script( 'lg-script' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );

function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

?>