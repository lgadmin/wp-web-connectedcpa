<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="site-content" role="main">
			<main>

				<?php
					$feature_image = get_field('feature_image');
				?>
				<div id="feature-image">
					<img class="img-full" src="<?php echo $feature_image['url']; ?>" alt="<?php echo $feature_image['alt']; ?>">
				</div>

				<!-- About us -->
				<?php
					$about_us = get_field('about_us');
				?>
				<div id="about-us" class="bg-blue">
					<div class="py-5 text-white container">
						<?php echo $about_us; ?>
					</div>
				</div>
				<!-- end About us -->

				<!-- Our Team -->
				<div id="our-team" class="bg-gray py-5">
					<div class="container">
						<h2 class="mb-4 text-center">Our Team</h2>
					
						<?php
						if( have_rows('our_team') ):
							?>
							<div class="team-list row">
							<?php
						    while ( have_rows('our_team') ) : the_row();
						        $image = get_sub_field('image');
						        $name = get_sub_field('name');
						        $position = get_sub_field('position');
						        $short_description = get_sub_field('short_description');
						        ?>
								<div class="col-md-3 col-sm-6">
									<div class="head-image">
										<img class="img-full" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
										<div class="description">
											<?php echo $short_description; ?>
										</div>
									</div>
									<div class="details py-3 text-center">
										<div class="h5 mb-1 text-blue"><?php echo $name; ?></div>
										<div><?php echo $position; ?></div>
									</div>
								</div>
						        <?php
						    endwhile;
						    ?></div><?php
						else :
						    // no rows found
						endif;
						?>
					</div>
				</div>
				<!-- end Our Team -->

				<!-- Services -->
				<?php
					$service_background_image = get_field('service_background_image');
					$services_description = get_field('services_description');
				?>

				<div id="our-services" class="py-5 text-white" style="background-image: url('<?php echo $service_background_image; ?>');">
					<div class="container">
						<h2 class="mb-4 text-center">Services</h2>
						<div class="text-center"><?php echo $services_description; ?></div>
						<?php
						if( have_rows('services_list') ):
							?>
							<div class="service-list row mt-5">
							<?php
						    while ( have_rows('services_list') ) : the_row();
						        $image = get_sub_field('image');
						        $name = get_sub_field('name');
						        ?>
								<div class="col-md-4 col-sm-6 px-md-5 text-center">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
									<div class="text-white mt-2 text-center"><?php echo $name; ?></div>
								</div>
						        <?php
						    endwhile;
						    ?></div><?php
						else :
						    // no rows found
						endif;
						?>
					</div>
				</div>
				<!-- end Services -->

				<!-- Testimonials -->
				<div id="testimonials" class="py-5 bg-gray">
					<div class="container">
						<?php echo do_shortcode('[lg-testimonial]'); ?>
					</div>
				</div>
				<!-- end Testimonials -->

				<!-- Location -->
				<div id="location">
					<div class="bg-dark py-5 text-white">
						<div class="container">
							<h2 class="mb-0 text-center">Our Location</h2>
						</div>
					</div>

					<div class="google-map">
						<div class="row no-gutters">
							<div class="map col-md-7 col-lg-8">
								<?php echo do_shortcode('[lg-map id=290]'); ?>
							</div>
							<div class="address col-md-5 col-lg-4 px-3 py-5 py-md-3 d-flex justify-content-center align-items-center flex-column">
								<?php echo do_shortcode('[lg-site-logo]'); ?>
								<div class="mt-4">
									<?php echo do_shortcode('[lg-address1]'); ?><br />
									<?php echo do_shortcode('[lg-city]'); ?> <?php echo do_shortcode('[lg-province]'); ?>, <?php echo do_shortcode('[lg-postcode]'); ?><br /><br />
									Phone: <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a><br />
									Fax: <?php echo do_shortcode('[lg-fax]'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end Location -->

				<!-- CTA -->
				<?php
					$cta_content = get_field('cta_content');
					$cta_button = get_field('cta_button');
				?>
				
				<div class="py-4 bg-green">
					<div class="container">
						<div class="d-flex flex-column flex-md-row justify-content-center justify-content-md-between align-items-center">
							<div class="mb-4 mb-md-0">
								<h2 class="h4 mb-0 text-white"><?php echo $cta_content; ?></h2>
							</div>
							<div><a class="btn btn-blue" href="<?php echo $cta_button['url']; ?>"><?php echo $cta_button['title']; ?></a></div>
						</div>
					</div>
				</div>
				<!-- end CTA -->

				<!-- Connect -->
				<?php
					$connect_subtitle = get_field('connect_subtitle');
					$connect_image = get_field('connect_image');
				?>
				<div id="connect" class="py-5">
					<div class="container">
						<h2 class="h3 mb-1 text-center">Connect With Us</h2>
						<p class="text-center"><?php echo $connect_subtitle; ?></p>

						<div class="pt-md-5 row">
							<div class="py-3 py-md-0 col-md-5 d-flex flex-column align-items-center justify-content-center">
								<ul class="list-unstyled">
									<li><span class="icon"><i class="fa fa-building" aria-hidden="true"></i></span><span>
										<?php echo do_shortcode('[lg-address1]'); ?><br />
										<?php echo do_shortcode('[lg-city]'); ?> <?php echo do_shortcode('[lg-province]'); ?>, <?php echo do_shortcode('[lg-postcode]'); ?>
									</span></li>
									<li><span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span><span>
										<a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a>
									</span></li>
									<li><span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span><span>
										<a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a>
									</span></li>
								</ul>
							</div>
							<div class="col-md-7">
								<img class="img-full" src="<?php echo $connect_image['url']; ?>" alt="<?php echo $connect_image['alt']; ?>">
							</div>
						</div>
					</div>
				</div>
				<!-- end Connect -->

				<!-- Contact us -->
				<div id="contact-us" class="bg-blue py-5">
					<div class="container">
						<h2 class="h3 text-white text-center">
							Contact Us Today
						</h2>
						<div class="form">
							<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
						</div>
					</div>
				</div>
				<!-- end Contact Us -->

			</main>
		</div>
	</div>

	<div class="modal-wrapper">
		<div>
			<div class="modal-content">

			</div>
		</div>
	</div>

<?php get_footer(); ?>