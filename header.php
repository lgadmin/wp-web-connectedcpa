<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="site-branding mt-md-4 px-md-5">
			<div class="social-media d-none d-md-block">
				<!-- Placeholder for space -->
				<a class="btn btn-blue text-center py-1" style="visibility: hidden;" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><span class="h5 text-white">CALL NOW</span><br /><?php echo do_shortcode('[lg-phone-main]'); ?></a>
			</div>
			<div class="logo">
				<?php echo do_shortcode('[lg-site-logo]'); ?>
			</div>
			<div class="call-now d-none d-md-block">
				<a class="btn btn-blue text-center py-1" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><span class="h5 text-white">CALL NOW</span><br /><?php echo do_shortcode('[lg-phone-main]'); ?></a>
			</div>

		 	<div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">

  		<?php wp_nav_menu( array(
        	'theme_location'              => 'top-nav',
    		'depth'             => 2,
    		'container'         => 'div',
    		'container_class'   => 'collapse navbar-collapse d-md-flex pt-4',
    		'container_id'      => 'main-navbar',
    		'menu_class'        => 'nav',
    		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    		'walker'            => new WP_Bootstrap_Navwalker()
    		)
      	); 

      	?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
