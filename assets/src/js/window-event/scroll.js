// Window Scroll Handler

repositionStickyHeader();

if($(window).scrollTop() > 150 && !$('.site-header').hasClass('scroll')){
	$('.site-header').addClass('scroll');
}else if($(window).scrollTop() <= 150 && $('.site-header').hasClass('scroll')){
	$('.site-header').removeClass('scroll');
}